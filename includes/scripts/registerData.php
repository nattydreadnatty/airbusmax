<?php

const HTTP_OK = 200;
const HTTP_BAD_REQUEST = 400;
const HTTP_METHOD_NOT_ALLOWED = 405;

function getCity($latitude, $longitude){
    $geolocation = $latitude.','.$longitude;
    
    $request = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='.$geolocation.'&sensor=false&language=fr&result_type=locality&key=AIzaSyCOK0Q1fbQOWFCuumTw9uc3G05WKFD9zdY'; 
    $json = file_get_contents($request);
    $data = json_decode($json);

    if($data) {
        
        $city= strval($data->results[0]->address_components[0]->long_name);
            
     } else{
       $city = 'Location Not Found';
     }

    return $city;
}


if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtoupper($_SERVER['HTTP_X_REQUESTED_WITH']) == 'XMLHTTPREQUEST') {
    // PREVENIR INJECTION ICI 
    $response_code = HTTP_BAD_REQUEST;
    $message = "Il manque le paramètre FLIGHT";

    if($_POST['action'] == 'showData' && isset($_POST['flight']) && isset($_POST['meteo'])) {
        $response_code = HTTP_OK;
        $flight = $_POST['flight'];
        $meteo = $_POST['meteo'];

        $city = getCity($flight['lattitude'], $flight['longitude']);

        $message = '<h5>Données reçues !</h5>';

        $pdo = require 'connect.php';

        $data = [

            'icao24' => $flight['icao'],
            'callsign' => $flight['callsign'],
            'origin_country' => $flight['origin_country'],
            'time_position' => $flight['time_position'],
            'last_contact' => $flight['last_contact'],
            'longitude' => $flight['longitude'],
            'lattitude' => $flight['lattitude'],
            'city' => $city,
            'velocity' => $flight['velocity'],
            'geo_altitude' => floatval($flight['geo_altitude']),
        ];

        $sql = "INSERT INTO vols (icao24, callsign, origin_country, time_position, last_contact, longitude, lattitude, city, velocity, geo_altitude) 
                VALUES (:icao24, :callsign, :origin_country, :time_position, :last_contact, :longitude, :lattitude, :city, :velocity, :geo_altitude)";
        $pdo->prepare($sql)->execute($data);

        $flightId = $pdo->lastInsertId();

        $dataMeteo = [
            'img' => $meteo['image'],
            'temperature' => floatval($meteo['temperature']),
            'meteo_description' => $meteo['description'],
            'volId' => $flightId
        ];

        $sqlMeteo = "INSERT INTO meteo (img, temperature, meteo_description, volId)
                     VALUES (:img, :temperature, :meteo_description, :volId)";
        $pdo->prepare($sqlMeteo)->execute($dataMeteo);

        $pdoMessage = '<h5 id="info-user-titre" style="font-family:\'PT Sans Narrow\', sans-serif; font-weight:100;font-size:20px;">Le vol '.$flight['icao'].' en provenance de ' .$flight['origin_country']. ' a bien été enregistré !</h5>';
    
        $pdo = null;
    }

    response($response_code, $message, $flight, $meteo, $pdoMessage, $pdo);
} else {
    $response_code = HTTP_METHOD_NOT_ALLOWED;
    $message = "Method not allowed!";

    response($response_code, $message);
}

function response($response_code, $response, $flight = null, $meteo = null, $pdoMessage = null, $pdo = null) {
    header('Content-Type: application/json');
    http_response_code($response_code);

    $response = [
        "response_code" => $response,
        "message" => $response_code,
        "flight" => $flight,
        "meteo" => $meteo,
        "pdoMessage" => $pdoMessage,
        "pdo" => $pdo
    ];

    echo json_encode($response);


}